import Vuex from 'vuex'
import database from '~/plugins/saveData'
import monitoreo from '~/plugins/monitoreo'
import cotizacion from '~/plugins/cotizacion'
import saveProspectoCRMService from '~/plugins/CRM'
const createStore = () => {
    const conexionCRMService =  new saveProspectoCRMService();
    return new Vuex.Store({
        state: {
            cargandocotizacion: false,
            msj: false,
            msjEje: false,
            config: {
                aseguradora: '',
                cotizacion: true,
                emision: true,
                descuento: 0,
                telefonoAS: "47495114",
                grupoCallback: '',
                from: '',
                idPagina: 0,
                img: "qualitas.webp",
                alert: true,
                asgNombre: "Quálitas",
                promo: "Obtén hasta",
                desc: "20% de descuento +",
                msi: "3 y 6 Meses Sin Intereses",
                dominio: "qualitasseguros.mx",
                domV2: "qualitasautos.mx",
                domV3: "qualitassegurosautos.com"
            },
            ejecutivo: {
                nombre: '',
                correo: '',
                id: 0
            },
            formData: {
                aseguradora: '',
                marca: '',
                modelo: '',
                descripcion: '',
                subDescripcion: '',
                detalle: '',
                detalleid: '',
                codigoPostal: '',
                nombre: '',
                apellidoPaterno: '',
                apellidoMaterno: '',
                telefono: '',
                correo: '',
                edad: '',
                fechaNacimiento: '',
                genero: '',
                precio: ''
            },
            solicitud: {},
            cotizacion: {
                "Cliente": {
                    "FechaNacimiento": null,
                    "Genero": null,
                    "Direccion": {
                        "CodPostal": null,
                        "Calle": null,
                        "Colonia": null,
                        "Poblacion": null,
                        "Ciudad": null,
                        "Pais": null
                    },
                    "Edad": null,
                    "TipoPersona": null,
                    "Nombre": null,
                    "ApellidoPat": null,
                    "ApellidoMat": null,
                    "RFC": null,
                    "CURP": null,
                    "Telefono": null,
                    "Email": null
                },
                "Cotizacion": {
                    "PrimaTotal": 0
                },
                "Emision": {
                    "PrimaTotal": null,
                },
                "Vehiculo": {
                    "Descripcion": null,
                    "Marca": null,
                    "Modelo": null,
                },
                "Aseguradora": "-",
                "Coberturas": [
                    {
                        "DanosMateriales": "-",
                        "DanosMaterialesPP": "-",
                        "RoboTotal": "-",
                        "RCBienes": "-",
                        "RCPersonas": "-",
                        "RC": "-",
                        "RCFamiliar": "-",
                        "RCExtension": "-",
                        "RCExtranjero": "-",
                        "RCPExtra": "-",
                        "AsitenciaCompleta": "-",
                        "DefensaJuridica": "-",
                        "GastosMedicosOcupantes": "-",
                        "MuerteAccidental": "-",
                        "GastosMedicosEvento": "-",
                        "Cristales": "-",
                    }
                ],
            },
            cotizacionesAmplia: [],
            cotizacionesLimitada: [],
            servicios: {
                servicioDB: 'http://138.197.128.236:8081/ws-autos/servicios'
            },
            prospecto : {
                numero: '',
                correo: '',
                nombre: '',
                sexo: '',
                edad: ''
            },
            productoSolicitud: {
                idProspecto: '',
                idTipoSubRamo: 1,
                datos: ''
            },
            cotizaciones: {
                idProducto: '',
                idPagina: '',
                idMedioDifusion: '',
                idEstadoCotizacion: 1,
                idTipoContacto: 1
            },
            cotizacionAli: {
                idCotizacion: '',
                idSubRamo: '',
                peticion: '',
                respuesta: '',
            },
            solicitudes: {
                idCotizacionAli: '',
                idEmpleado: '',
                idEstadoSolicitud: 1,
                idEtiquetaSolicitud: 1,
                idFlujoSolicitud: 1 ,
                comentarios: ''
            }
        },
        mutations: {
            cotizar: function (state) {
                cotizacion.search(
                    state.config.aseguradora,
                    state.formData.detalleid,
                    state.formData.codigoPostal,
                    state.formData.descripcion,
                    state.config.descuento,
                    state.formData.edad,
                    ("01/01/" + (new Date().getFullYear() - state.formData.edad).toString()).toString("yyyyMM/dd"),
                    (state.formData.genero === 'M' ? 'MASCULINO' : 'FEMENINO'),
                    state.formData.marca,
                    state.formData.modelo,
                    'cotizacion',
                    'AMPLIA',
                    'PARTICULAR'
                ).then(resp => {
                    console.log("Se esta Cotizando va bien");
                    state.cotizacion = resp;
                    if (typeof resp != "string") {
                        if (Object.keys(resp.Cotizacion).includes('PrimaTotal') && resp.Cotizacion.PrimaTotal != null && resp.Cotizacion.PrimaTotal != '') {
                            state.formData.precio = state.cotizacion.Cotizacion.PrimaTotal;
                            this.commit('saveData');
                            console.log('Exito al cotizar con ' + state.config.aseguradora);
                            this.commit('limpiarCoberturas');
                        }
                        else {
                            state.msj = true
                            this.commit('saveData');
                            console.log(state.config.aseguradora + " NO trajo Precios");
                        }
                    }
                    else {
                        state.msj = true
                        this.commit('saveData');
                        console.log("Problema al cotizar con " + state.config.aseguradora);
                    }
                });
            
            },
            limpiarCoberturas(state) {
                if (state.cotizacion.Coberturas) {
                    for (var c in state.cotizacion.Coberturas[0]) {
                        if (state.cotizacion.Coberturas[0][c] === "-") {
                            delete state.cotizacion.Coberturas[0][c];
                        }
                    }
                }
            },
            saveData: function (state) {
                console.log('Guardando Datos');
                database.search(
                    state.formData.modelo,
                    state.formData.apellidoMaterno,
                    state.formData.apellidoPaterno,
                    state.formData.correo,
                    state.formData.codigoPostal,
                    ((new Date().getFullYear() - state.formData.edad).toString()).toString("yyyyMM/dd"+"/01/01"),
                    state.config.grupoCallback,
                    state.config.idCampana,
                    state.config.idPagina,
                    state.formData.marca,
                    state.formData.nombre,
                    (state.formData.genero === 'MASCULINO' ? 'M' : 'F'),
                    state.formData.descripcion,
                    state.formData.subDescripcion,
                    state.formData.detalle,
                    state.formData.telefono,
                    state.config.telefonoAS,
                    state.config.from,
                    state.formData.precio)
                    .then(resp => {
                        state.solicitud = resp;
                        state.ejecutivo.nombre = resp.nombreEjecutivo;
                        state.ejecutivo.correo = resp.mailEjecutivo;
                        state.ejecutivo.id = resp.idEjecutivo;
                        this.commit('valid');
                        this.commit('saveMonitoreo');
                        this.commit('saveProspectoCRM');
                    });
            },
            saveMonitoreo: function (state) {
                console.log('save Monitoreo');
                monitoreo.search(
                    state.formData.modelo,
                    state.config.aseguradora,
                    (state.cotizacion.length > 0 ? 'SI' : 'NO'),
                    state.formData.descripcion,
                    state.formData.detalle,
                    state.config.idCampana,
                    state.ejecutivo.id,
                    state.config.idPagina,
                    state.solicitud.idSolicitud,
                    state.formData.marca,
                    "COTIZACION",
                    state.formData.subDescripcion,
                    state.formData.precio);
            },
            valid: function (state) {
                console.log('Se mando correo a ' + state.formData.nombre);
                console.log('Cotizacion Completa');
                if (state.formData.precio === '' && state.ejecutivo.id === '') {
                    state.msj = true
                }
                if (state.formData.precio === '') {
                    state.msjEje = true
                } else {
                    state.cargandocotizacion = true
                }
            },
            saveProspectoCRM: function (state) {
                state.prospecto.numero = state.formData.telefono;
                state.prospecto.correo = state.formData.correo;
                state.prospecto.nombre = state.formData.nombre;
                state.prospecto.sexo = state.formData.genero;
                state.prospecto.edad = state.formData.edad;
                conexionCRMService.validarCorreo(state.prospecto.correo).then(
                    correoExist => {
                        if (typeof correoExist.data.correo === 'string') {
                            state.productoSolicitud.idProspecto = correoExist.data.id;
                            this.commit('saveProductoSolicitudCRM');
                        } else {
                            conexionCRMService.postProspecto(state.prospecto).then(resp => {
                                state.productoSolicitud.idProspecto = resp.data.id;
                                this.commit('saveProductoSolicitudCRM');
                            })
                        }
                    }
                )
            },
            saveProductoSolicitudCRM: function (state) {
                state.productoSolicitud.datos = JSON.stringify(state.formData);
                conexionCRMService.postProductoSolicitud(state.productoSolicitud).then( resp => {
                    state.cotizaciones.idProducto = resp.data.id;
                    this.commit('saveCotizacionesCRM');
                })
            },
            saveCotizacionesCRM: function (state) {
                state.cotizaciones.idMedioDifusion = state.config.idPagina;
                conexionCRMService.postCotizacion(state.cotizaciones).then( resp => {
                    state.cotizacionAli.idCotizacion = resp.data.id;
                    this.commit('saveCotizacionesAli'); 
                })
            },
            saveCotizacionesAli: function (state) {
                state.cotizacionAli.idSubRamo = state.config.idAseguradora;
                state.cotizacionAli.peticion = JSON.stringify(state.formData);
                state.cotizacionAli.respuesta = '{"propiedad": "vacia"}';
                conexionCRMService.postCotizacionAli(state.cotizacionAli).then( resp => {
                    state.solicitudes.idCotizacionAli = resp.data.id;
                    this.commit('saveSolicitudes');
                })
            },
            saveSolicitudes: function (state) {
                state.solicitudes.idEmpleado = state.ejecutivo.id;
                console.log(state.solicitudes);
                conexionCRMService.postSolicitud(state.solicitudes).then( resp => {
                    console.log('CONEXION A TRIGARANTE2020 TERMINADA')
                })
            }
        }
    })
};
export default createStore
